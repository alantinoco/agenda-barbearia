const { Client, LocalAuth } = require('whatsapp-web.js');
const CalendarService = require('./calendar-service');
var qrcode = require('qrcode-terminal');

const client = new Client({
    authStrategy: new LocalAuth(),
    puppeteer:{headless:true}
});
client.on('loading_screen', (percent, message) => {
    console.log('LOADING SCREEN', percent, message);
});
client.on('qr', (qr) => {
    console.log(qr)
    
    qrcode.generate(qr, {small: true});
    
});

client.on('ready', () => {
    console.log('Client is ready!');
    client.sendMessage('5521969328610@c.us',"Ready to fly")
});

client.on('message', msg => {
    if (msg.body == '!ping') {
        msg.reply('pong');
    }
    
});

client.initialize();