const {google} = require('googleapis');
require('dotenv').config();

const CREDENTIALS = JSON.parse(process.env.CREDENTIALS);
const calendarId = process.env.CALENDAR_ID;

const SCOPES = 'https://www.googleapis.com/auth/calendar';
const calendar = google.calendar({version : "v3"});


const auth = new google.auth.JWT(
    CREDENTIALS.client_email,
    null,
    CREDENTIALS.private_key,
    SCOPES
);

let event = {
    'summary': `Alan`,
    'description': `barba`,
    'start': {
        'dateTime': '2022-10-27T17:45:00.000Z',
        'timeZone': 'America/Sao_Paulo'
    },
    'end': {
        'dateTime': '2022-10-27T18:30:00.000Z',
        'timeZone': 'America/Sao_Paulo'
    }
}

// INSERT
const insertEvent = async (event) => {

    try {
        let response = calendar.events.insert({
            auth: auth,
            calendarId: calendarId,
            resource: event
        });
        // && response['statusText'] === 'OK'
        if (response['status'] == 200){
            return 1;
        } else {
            return 0;
        }
    } catch (error) {
        console.log(`Error at insertEvent --> ${error}`);
        return 0;
    }
};
// insertEvent(event);


// GET
const getEvents = async (startTime, endTime) => {
    const calendarId = process.env.CALENDAR_ID;
    const start = startTime;
    const end = endTime;
    try {
        let response = await calendar.events.list({
            auth: auth,
            calendarId: calendarId,
            timeMin: start,
            timeMax: end,
            timeZone: 'America/Sao_Paulo'
        });
        let data = response.data.items;
        for (let i = 0; i < data.length; i++) {
            let events = response.data.items[i];
            console.log(`Cliente: ${events.summary}\nServiço: ${events.description}\nData: ${events.start.dateTime}\nIdentificador: ${events.id}\n`)
        }
    } catch (error) {
        console.log(`Error at getEvents --> ${error}`);
        return 0;
    }
};
let start = '2022-10-27T06:00:00.000Z';
let end = '2022-10-27T23:00:00.000Z';
getEvents(start, end);



// GET BY ID
const getEventsByID = async (id) => {

    try {
        let response = await calendar.events.list({
            auth: auth,
            calendarId: calendarId,
            id: id
        });

        let data = response.data.items;
        for (let i = 0; i < data.length; i++) {
            if(data[i].id == id){
                console.log("Seu agendamento está confirmado!\nData: ",data[i].start.dateTime,"\nServiço: ",data[i].description);
            }
        }
        console.log("Não foi encontrado nehum agendamento com esse identificador.\nDeseja fazer um agendamento?\nDigite: \"agendar\"");        
    } catch (error) {
        console.log(`Error at getEvents --> ${error}`);
    }
};
//getEventsByID("iik13c5oruhct3ml8kvepkmk6s");


// DELETE
const deleteEvent = async (eventId) => {

    try {
        let response = await calendar.events.delete({
            auth: auth,
            calendarId: calendarId,
            eventId: eventId
        });

        if (response.data === '') {
            return 1;
        } else {
            return 0;
        }
    } catch (error) {
        console.log(`Error at deleteEvent --> ${error}`);
        return 0;
    }
};

//let eventId = '2hd1pddiha4bpr93nv6uqrl22j';
//deleteEvent(eventId)
//    .then((res) => {
//        console.log(res);
//    })
//    .catch((err) => {
//        console.log(err);
//    });



