const GoogleCalendarService = require('./google-calendar');
class CalendarService{

    CalendarService(){
        this.googleCalendarService = GoogleCalendarService();
    }
    insertEvent(name,service, dateHour){
        var start = new Date( Date.parse(dateHour));
        var endHour = new Date(start);
        let timeForService = this.#getTimeForServices(service)
        endHour.setMinutes(start + timeForService);
        let event = {
            'summary': `${name}`,
            'description': `${service}`,
            'start': {
                'dateTime': start.toISOString,
                'timeZone': 'America/Sao_Paulo'
            },
            'end': {
                'dateTime': endHour.toISOString,
                'timeZone': 'America/Sao_Paulo'
            }
        };
        this.googleCalendarService.insertEvent(event);
    }

    #getTimeForServices(service){
        switch (service.toLowerCase()){
            case "maquina":
                return 25;
            case "barba":
                return 30;
            default:
                return 60;
        }
            
    }


}
module.exports = new CalendarService();